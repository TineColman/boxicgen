# SCRIPT TO CREATE INITIAL CONDITIONS FOR ISOTHERMAL TURBULENT BOX SIMULATIONS

mydir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# PARAMETERS (change parameters here)

# name of the simulation
name=box3
#name=$4

# refinement levels
lvl_min=8
lvl_max_IC=9
lvl_max=9

# cloud size (in c.u.)
size="0.25"
# Temperture
Temp="10"
# molecular weight
mu="2.37"

# velocity rescaling options:
#option_vel="Mach_number1D"
option_vel="Mach_number3D"
#option_vel="Larson"
#option_vel="dispersion"
# value for 3D Mach number or Larson parameter (arbitary)
value_vel=18

# Mass normalisation options
#option_mass="Virial"
option_mass="Fixed_mass"
#option_mass="Fixed_density"
# value for virial or mass or density
value_mass="260"

#name=L"$size"_rho"$value_mass"_sigma"$value_vel"

# random seeds for MUSIC
s1=35588
s2=03573
s3=81985
#s1=$1
#s2=$2
#s3=$3
comp1="u"
comp2="v"
comp3="w"

# STEP 1: generate random velocity field with MUSIC at the highest resolution
# make directories
mkdir ic_"$name"_"$s1"_"$s2"_"$s3"
cd ic_"$name"_"$s1"_"$s2"_"$s3"
mkdir ic_lvl_"$lvl_max_IC"
cd ic_lvl_"$lvl_max_IC"

#: <<'END'
# construct config files for MUSIC
echo -e "\e[31mConstructing MUSIC input files...\e[0m"
echo "Using seeds " $s1 $s2 $s3
python $mydir/write_music_conf.py $(($lvl_max_IC)) $comp1 $(($s1))
python $mydir/write_music_conf.py $(($lvl_max_IC)) $comp2 $(($s2))
python $mydir/write_music_conf.py $(($lvl_max_IC)) $comp3 $(($s3))

# generate x, y and z component of the velocity field
echo -e "\e[31mGenerating raw velocity field...\e[0m"
MUSIC music_ic_u.conf > output_music_u.txt
MUSIC music_ic_v.conf > output_music_v.txt
MUSIC music_ic_w.conf > output_music_w.txt

# move usefull files
mv raw_ic_u/level_0"$(($lvl_max_IC/10))""$(($lvl_max_IC%10))"/ic_deltab ic_vel_u
mv raw_ic_v/level_0"$(($lvl_max_IC/10))""$(($lvl_max_IC%10))"/ic_deltab ic_vel_v
mv raw_ic_w/level_0"$(($lvl_max_IC/10))""$(($lvl_max_IC%10))"/ic_deltab ic_vel_w
# remove unnecessary files
rm -r raw_ic_u
rm -r raw_ic_v
rm -r raw_ic_w
rm -r wnoise_00"$(($lvl_max_IC/10))""$(($lvl_max_IC%10))".bin

#END

# STEP 2: rescale the velocity field according to the desired conditions
#         generate density and pressure initial condition
echo -e "\e[31mRescaling velocity fields and creating initial density and pressure...\e[0m"
#python $mydir/rescale_fields.py $(($lvl_max_IC)) $size $Temp $mu $option_vel $((value_vel)) $option_mass $(($value_mass))
python $mydir/rescale_fields.py $(($lvl_max_IC)) $size $Temp $mu $option_vel $value_vel $option_mass $value_mass

# remove unnecessary files
rm ic_vel_u
rm ic_vel_v
rm ic_vel_w
rm music*
rm output*
rm input_powerspec.txt 


cd ..

# STEP 3: degrade max level ICs
echo -e "\e[31mDegrading ICs...\e[0m"

lvl=$(($lvl_max_IC - 1))
# while lvl greater or equal to lvl_min
while [ $lvl -ge $lvl_min ]
do
    mkdir ic_lvl_"$lvl"
    lvl_old=$(($lvl + 1))
    $mydir/degrade_grafic ic_lvl_"$lvl_old" ic_lvl_"$lvl"
    lvl=$(( $lvl - 1 ))
done

cd ..


